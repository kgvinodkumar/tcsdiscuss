// Notice that we do not have a controller since this component does not
// have any specialized logic.

export default {
  name : 'statusList',
  config : {
    templateUrl      : 'src/users/components/status/StatusList.html'
  }
};
